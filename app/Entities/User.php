<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\softDeletes;//add pelo prof
use Illuminate\Foundation\Auth\User as Authenticatable;

/*Eloquent é o ORM que o Laravel usa*/

class User extends Authenticatable
{
    use softDeletes;//add pelo prof, requer importar antes
    use Notifiable;

    public $timestamps = true;//add pelo prof
    protected $table   ='users';//add pelo prof (citou tbl_usuario em aula 7 14:45)

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *parametros que se devem passar ao instanciar a classe, caso seja sem
     *parametros, cada atributo deverá ser passado individualmente depois de
     *instanciada a classe:
     *usuario = new User('NomeDoCara','email', 'adadadad')
     *
     *$fillable = [];//vazio
     *usuario  = new User();
     *usuario->name = "NomeDoCara";
     *usuario->email = "email";
     *
     *Não passar no fillable campos que o laravel já trata, como o id, updated_at, etc
     */
    protected $fillable = ['cpf', 'name', 'phone','birth','gender','notes','email','password','status','permission',];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     *aqui são definidos os campos que ficam ocultos numa consulta a tabela Users
     */
    protected $hidden = ['password', 'remember_token',];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['email_verified_at' => 'datetime',];

    public function setPasswordAttribute($value){
      $this->attributes['password'] = env('PASSWORD_HASH') ? bcrypt($value) : $value;
    }
}
