<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function homepage(){
      $variavel = "homepage do sistema de gestão de grupos de investimentos";
      return view ('welcome',['title'=>$variavel]);
    }

    public function cadastrar(){
        echo  "Tela de cadastro";
    }

    /*
    *method to user login view
    */
    public function fazerLogin(){
      echo "Tela de login";

    }
    public function login(){
      return view('user.login'); //pasta.nomeView (ex. /resources/user/login.blade.php)

    }

}
