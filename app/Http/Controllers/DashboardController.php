<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Exception;
use Auth;

class DashboardController extends Controller
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index(){
      return "estamos na index (Dashboard)";
    }

    public function auth(Request $request){
        $data = [
          'email'    => $request->get('username'),
          'password' => $request->get('password')
        ];
        try {
          if(env('PASSWORD_HASH'))
          {
              Auth::attempt($data,false);//dados do post, salvar cache de login false
          }//fim if
          else
          {
            $user = $this->repository->findWhere(['email' => $request->get('username')]);

            if(!$user)
                throw new Exception("email informado inválido");

            if($user->password != $request->get('password'))
                throw new Exception("senha informada inválida");

            Auth::login($user);
          }//fim else


          return redirect()->route('user.dashboard');
        } catch (Exception $e) {
            return $e->getMessage();
        }


    //  dd($request->all());
    }

}
