<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
          'cpf'      => '11122233345',
          'name'     => 'Jõao',
          'phone'    => '3599999999',
          'birth'    => '1980-10-01',
          'gender'   => 'M',
          'email'    => 'joao2@sistema.com',
          'password' => env('PASSWORD_HASH') ? bcrypt('123456') : '123456',//se no env tiver como true ou false, criptografa ou nao a senha q vai pro banco

        ]);
    }
}
