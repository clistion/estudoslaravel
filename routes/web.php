<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/',['uses'=>'Controller@homepage']);//classe Controller@metodo
Route::get('/cadastro',['uses'=>'Controller@cadastrar']);
/*
*Rota de login do usuário
*
*/
//Route::get('/login',['uses'=>'Controller@fazerLogin']);
Route::get('/login',['as'=>'user.login','uses'=>'Controller@login']);//cahama view de login
Route::post('/login',['as'=>'user.login','uses'=>'DashboardController@auth']);//trata post da view login
Route::get('/dashboard',['as'=>'user.dashboard','uses'=>'DashboardController@index']);//trata redirect da view login
